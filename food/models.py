from django.contrib.auth.models import User
from django.db import models


class Food(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    ate_on_date = models.DateField()
    desc = models.TextField(blank=True)

    charfields = ['name','place','lat','lng']

    for f in charfields:
        locals()[f] = models.CharField(max_length=30, blank=True)


    def save(self, *args, **kwargs):
        super(Food, self).save(*args, **kwargs)


    class Meta:
        db_table = 'food'
