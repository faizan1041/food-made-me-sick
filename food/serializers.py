from rest_framework import serializers
from models import Food
from django.contrib.auth.models import User


class FoodSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = Food
        fields = tuple([x.name for x in Food._meta.get_fields(include_hidden=True)])
